import pytest

from pysnake.engine import Engine


A_FPS = 25


@pytest.fixture
def one_loop_end(mocker):
    return mocker.Mock(side_effect=[False, True])


@pytest.mark.timeout(1)
def test_engine_end_game_break_game_loop(mocker, one_loop_end):
    engine = Engine(one_loop_end, mocker.Mock())

    engine.main()

    assert True


@pytest.mark.timeout(1)
def test_engine_sleep_residual_time(mocker, one_loop_end):
    tick_counter = mocker.Mock()
    engine = Engine(one_loop_end, tick_counter)

    engine.main()

    assert tick_counter.sleep.call_count == 1
