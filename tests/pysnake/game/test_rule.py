from pysnake.game.actors import Snake, Apple, Position
from pysnake.game.rules import check_apple_eat


def test_snake_head_occupy_apple_score_goes_up():
    snake = Snake(Position(0, 0))
    apple = Apple(Position(0, 0))

    is_apple_eaten = check_apple_eat(snake, apple)

    assert is_apple_eaten
