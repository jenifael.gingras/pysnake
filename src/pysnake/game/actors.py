from dataclasses import dataclass


@dataclass
class Position:
    x: int
    y: int

    def __add__(self, other):
        """
        :type other: Position
        """
        self.x += other.x
        self.y += other.y
        return self


@dataclass
class Snake:
    head: Position


@dataclass
class Apple:
    position: Position
