def check_apple_eat(snake, apple):
    """
    Given a snake and an apple, check if the apple is eaten by the snake.
    :param snake: pysnake.game.actors.Snake
    :param apple: pysnake.game.actors.Apple
    :rtype: bool
    """
    return apple.position == snake.head
