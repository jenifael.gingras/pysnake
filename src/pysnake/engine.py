class End:
    def __call__(self, *args, **kwargs):
        return True


class TickCounter:
    def __init__(self, fps, time):
        """
        :type fps: int
        :type time: time
        """
        self.fps = fps
        self._time = time
        self._tick = 0

    def tick(self):
        self._tick = self._time.time()

    def sleep(self):
        dt = self._time.time() - self._tick
        sleep_time = 1/self.fps - dt
        self._time.sleep(sleep_time)


class Engine:
    def __init__(self, end, tick_counter):
        """
        :type end: End
        :type tick_counter: TickCounter
        """
        self._end = end
        self._commands = []
        self._tick_counter = tick_counter

    def main(self):
        while not self._end():
            self._tick_counter.tick()

            self.commands()
            self.update()

            self._tick_counter.sleep()
            self.render()

    def commands(self):
        self._commands = []

    def update(self):
        pass

    def render(self):
        pass
