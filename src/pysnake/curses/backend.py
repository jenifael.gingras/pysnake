import copy
import curses
import random
from curses import wrapper
import time

from pysnake.game.actors import Position


FPS = 15
MIN_LINE = 3
MIN_COLUMN = 3
MAX_LINE = MIN_LINE + 24
MAX_COLUMN = MIN_COLUMN + 80

SNAKE_COLOR = 1
APPLE_COLOR = 2
BORDER_COLOR = 3
TITLE_COLOR = 4


def main(stdscr):
    _init()
    stdscr.clear()
    stdscr.nodelay(True)

    running = True
    score = 0
    snake = [Position(10, 10)]
    apple = _make_apple()
    move_vector = Position(0, 0)
    while running:
        # Drawing
        stdscr.clear()
        for i in range(MIN_COLUMN - 1, MAX_COLUMN + 2):
            stdscr.addstr(MIN_LINE - 1, i, " ", curses.color_pair(BORDER_COLOR))
            stdscr.addstr(MAX_LINE + 1, i, " ", curses.color_pair(BORDER_COLOR))
        for j in range(MIN_LINE - 1, MAX_LINE + 2):
            stdscr.addstr(j, MIN_COLUMN - 1, " ", curses.color_pair(BORDER_COLOR))
            stdscr.addstr(j, MAX_COLUMN + 1, " ", curses.color_pair(BORDER_COLOR))

        stdscr.addstr(MIN_LINE - 2, MIN_COLUMN - 1, "Dark Snake!", curses.color_pair(TITLE_COLOR))
        stdscr.addstr(MAX_LINE + 2, MIN_COLUMN - 1, "Score: {}".format(score), curses.A_REVERSE)
        for part in snake:
            stdscr.addstr(part.y, part.x, " ", curses.color_pair(SNAKE_COLOR))
        stdscr.addstr(apple.y, apple.x, " ", curses.color_pair(APPLE_COLOR))
        stdscr.refresh()

        # Input
        time.sleep(1 / FPS)
        input = stdscr.getch()

        # Update

        # Move snake
        original_snake = copy.deepcopy(snake)
        if input == curses.KEY_LEFT:
            move_vector = Position(0, -1)
        elif input == curses.KEY_RIGHT:
            move_vector = Position(0, 1)
        elif input == curses.KEY_UP:
            move_vector = Position(-1, 0)
        elif input == curses.KEY_DOWN:
            move_vector = Position(1, 0)
        elif input == ord('q'):
            break

        # kill snake
        snake_head = snake[0]
        snake_head += move_vector if move_vector else Position(0, 0)
        snake[0] = snake_head
        for part in snake[1:]:
            if snake_head == part:
                running = False

        # eat apple
        if snake_head == apple:
            while apple in snake:
                apple = _make_apple()
            score += 1
            snake.append(copy.deepcopy(snake_head))

        # move snake body
        for idx, _ in enumerate(original_snake):
            if idx == 0:
                continue
            snake[idx] = original_snake[idx-1]

        # wrapping borders
        for part in snake:
            if part.y > MAX_LINE:
                part.y = MIN_LINE
            elif part.y < MIN_LINE:
                part.y = MAX_LINE

            if part.x > MAX_COLUMN:
                part.x = MIN_COLUMN
            elif part.x < MIN_COLUMN:
                part.x = MAX_COLUMN

    stdscr.nodelay(False)
    stdscr.clear()
    stdscr.addstr(MIN_LINE + int(MAX_LINE/2), MIN_COLUMN + int(MAX_COLUMN/2), "GAME OVER!", curses.A_REVERSE)
    stdscr.getch()
    return


def _make_apple():
    return Position(random.randint(MIN_LINE, MAX_LINE - 1), random.randint(MIN_COLUMN, MAX_COLUMN - 1))


def _init():
    curses.init_pair(SNAKE_COLOR, curses.COLOR_RED, curses.COLOR_RED)
    curses.init_pair(APPLE_COLOR, curses.COLOR_GREEN, curses.COLOR_GREEN)
    curses.init_pair(BORDER_COLOR, curses.COLOR_WHITE, curses.COLOR_WHITE)
    curses.init_pair(TITLE_COLOR, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.noecho()
    curses.curs_set(0)


if __name__ == '__main__':
    wrapper(main)
